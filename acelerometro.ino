#include <ADXL335.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

ADXL335 accelerometer;
HTTPClient http;
const char* ssid = "Familia Castellon Pineda 2";
const char* password = "Adri51797538";
String url = "/api";
String res="";
int httpCode=-1;
boolean addedData=false;



void setup(){
  Serial.begin(115200);
  Serial.println();
  pinMode(2,OUTPUT); 
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  http.begin("http://192.168.1.105:3000/api");
  http.addHeader("Content-Type", "application/json");
  Serial.println(" connected");
  
  pinMode(5,INPUT_PULLUP);
  accelerometer.begin();
}

void loop() {
  boolean button = digitalRead(5);
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonArray& data = root.createNestedArray("data");
  char JSONmessageBuffer[300];
  while(!button){
  button = digitalRead(5);
  float z = accelerometer.getAccelerationZ();
  data.add(z);
    delay (50);
    addedData=true;
  }
  if(WiFi.status()== WL_CONNECTED && addedData ){
    root.printTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    httpCode = http.POST(JSONmessageBuffer);    
    res = http.getString();
    root.printTo(Serial);
    Serial.println(res);
    http.end();
  }
  addedData=false;
}
